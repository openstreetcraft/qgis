# qgis

Dockerized [QGIS](https://qgis.org) desktop application to visualize data provided by geographic information systems.

## Start the application

```
./docker-compose.sh
```

## Add layers from an OpenStreetCraft WMS server

Enter the subproject which provides a WMS data source, like mapproxy, mapserver or geoserver.
Start the WMS server with docker-compose.

Import layers into QGIS by opening the "Add Layers from WMS Server" dialog with Ctrl-Shift-W.
Load the servers.xml file, select the appropriate server and press "Connect". 
